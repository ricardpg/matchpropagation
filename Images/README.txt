Dataset ./Aloe:

  Images can be downloaded from:
    http://vision.middlebury.edu/stereo/

  Aloe folder is:
    http://vision.middlebury.edu/stereo/data/scenes2006/FullSize/Aloe/

  A full copy of http://vision.middlebury.edu/stereo/data/scenes2006/
  is available on porcsenglar.udg.edu server in folder:
    /mnt/uvl/DataSets/Middlebury/scenes2006


Datasets ./Coral
         ./Trinocular

  These datasets are obtained by rectifying images from the datasets
  available in porcsenglar.udg.edu:
    /mnt/uvl/DataSets/Coral
    /mnt/uvl/DataSets/Trinocular/Pati-08-01-31


  Jordi Ferrer <jferrerp@eia.udg.edu>
