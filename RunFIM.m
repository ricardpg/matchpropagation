%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : Match Propagation Functions
%  Homepage      : http://uvl.udg.edu
%
%  Module        : FIM script to generate the initial Seed for Match Propagation
%
%  File          : RunFIM.m
%  Date          : 11/02/2008 - 12/02/2008
%  Encoding      : ISO-8859-1 (Latin-1)
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     :
%
%  Notes         :
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2008 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Clean
close all;

% Configuration
SetConfVars;

% FIM Input Parameters
%
Mask = [];

% Image Processing Parameters
%
InputFIM.Processing.SingleChannel = 4;                % 0-no, 1-red, 2-green, 3-blue, 4-Y channel of YIQ, 5-PCA
InputFIM.Processing.Equalization  = 1;                % 0-no, 1-normalization, 2-equalization, 3-CLAHE
InputFIM.Processing.ResizeCoeff   = 1;                % resize coefficient to decrease sizes of images
% InputFIM.Processing = 0;                            % <- to cancel Preprocessing

% Detection & Description Parameters
%
Detectors   = { 'SIFT',   'SURF', ...                 % 1 - 2  <- scale-invariant detectors
                'Harris', 'Hessian', 'Laplacian', ... % 3 - 5  <- non-scaleinvariant detectors
                'MSER' };                             % 6      <- region detector

Descriptors = { 'SIFT', 'SURF', 'SURF-128', ...       % 1 - 3
                'SURF-36', 'U-SURF', ...              % 4 - 5
                'U-SURF-128', 'U-SURF-36', ...        % 6 - 7
                'ImagePatch', 'RotatedImagePatch' };  % 8 - 9      

InputFIM.Detection.DetType          = Detectors(2);
InputFIM.Detection.MaxNumber        = 100000;         % max number of features to detect
InputFIM.Detection.RadiusNonMaximal = 8;              % radius for non-maximal suppression for detector
InputFIM.Detection.Sigma            = 2;              % sigma of the Gaussian used in Harris, Laplacian and Hessian detectors; If = 0, no smoothing is performed
InputFIM.Detection.MinCornerness    = 0.5;            % [0-1] cornerness threshold
InputFIM.Detection.MinArea          = 30;             % [30] min region area in pix for region detectors
InputFIM.Detection.DescType         = Descriptors(2);            

InputFIM.Description.DescType       = InputFIM.Detection.DescType;
InputFIM.Description.PatchRadius    = 14;             % radius of the image patch
% InputFIM.Detection = 0;                             % cancels Detection
% InputFIM.Description = 0;                           % <- to cancel description 

% Matching Parameters

Matchers = { 'DistRatio', 'Correlation' };

InputFIM.Matching.Matcher               = Matchers(1);
InputFIM.Matching.RatioValue            = 1.5;        % ratio between the next closest and closest nearest neighbour for SIFT matching ; the bigger is the ratio, the smaller is the amount of matches
InputFIM.Matching.CorrelationThreshold  = 0.9;
InputFIM.Matching.Bidirectional         = 1;          % If ~= 0 Intersection between "image1 to image2" and "image2 to image1" matches
InputFIM.Matching.GuidedPostMatching    = 1;
% InputFIM.Matching = 0;                              % cancels Matching
 
% Motion Estimation Parameters
%
HomographyModels  = { 'Euclidean', 'Similarity', 'Affine', ...   % 1 - 3
                      'Projective', 'ProjectiveNLM'  };          % 4 - 5

InputFIM.MotionEstimation.HomographyModel = HomographyModels(1);
InputFIM.MotionEstimation.SpatialStdev    = 2;        % stdev of spatial coord of features, in pixels
% InputFIM.MotionEstimation = 0;                      % cancels motion estimation

% Save Parameters
%
InputFIM.Save.Results     = 1;                        % 1 - save; 0 - do not save
InputFIM.Save.HomoInASCII = 1;                        % 1 - save; 0 - do not save
InputFIM.Save.SaveTo      = [ ResultsPath '/' ];
InputFIM.Save.TestName    = '';
% InputFIM.Save           = 0;

% Display Parameters
%
InputFIM.Display.DisplayConfig  = 1;                  % 1 -> display FIM configuretion; 0 -> not display
InputFIM.Display.DisplayResults = 1;                  % 1 -> display statistics of FIM execution results; 0 -> not display
InputFIM.Display.SepLines       = 1;                  % 1 -> display separation lines; 0 -> not display
% InputFIM.Display = 0;                               % <- to cancel display output of FIM

% Plotting Parameters
%
InPlotFIM.PlotInitialImages    = 1;                   % 1 -> initial images are displayed; 0 -> the processed grayscale images are displayed;
InPlotFIM.SaveFigures          = 1;             
InPlotFIM.SaveInFormat         = 'png';               % supported: 'jpg', 'tif', 'eps', 'png', 'ppm', 'wmf' ('wmf' only for Windows)
InPlotFIM.PlotImages           = 1;                   % 1 -> display images with keypoints; 0 -> not display
InPlotFIM.PlotInitialMatches   = 1;                   % 1 -> display initial matches; 0 -> not display
InPlotFIM.PlotRejectedMatches  = 1;                   % 1 -> display rejected by RANSAC matches; 0 -> not display
InPlotFIM.PlotAcceptedMatches  = 1;                   % 1 -> display accepted by RANSAC matches; 0 -> not display
InPlotFIM.PlotMotion           = 1;                   % 1 -> display matched pairs in the first image; 0 -> not display;
InPlotFIM.PlotMosaic           = 1;                   % 1 -> display mosaic of two images; 0 -> not display;
InPlotFIM.PlotFollowingMatches = 1;                   % 1 -> display all following matches;
InPlotFIM.WhereToSave          = [ ResultsPath '/' ]; 
InPlotFIM.PlotMatchedRegions   = 1;                   % 1 -> MSER only, regions borders will be displayed for Initial, Rejected and Accepted matches
InPlotFIM.PercentageToDisplay  = 100;                 % percentage of accepted mathces to display, useful when there is lots of matches
if isstruct(InputFIM.Processing)
  InPlotFIM.ResizeCoeff          = InputFIM.Processing.ResizeCoeff;
end
% InputPlotFIM = 0;                                   % suppress plotting


% FIM Execution
%
% Addition of Paths
addpath ( FIMPath );
SetPathsFIM ( FIMPath );


% Loading Images
Image1 = imread ( [ DataPath '/' ImageSubFolder{PairN} '/' ImageFiles{PairN,1} ] ); %#ok<USENS>
Image2 = imread ( [ DataPath '/' ImageSubFolder{PairN} '/' ImageFiles{PairN,2} ] );


% Executing FIM
[ Img1, Img2, ResultsFIM ] = FIM ( Image1, Image2, Mask, InputFIM );


% Plotting the FIM results
%
% Figs contains handles of Figures
Figs = PlotFIM ( Image1, Image2, Img1, Img2, ResultsFIM, InPlotFIM );


% Saving Results for Match Propagation Algorithm
save ( ResultsDataFilePath );
