%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : Match Propagation Functions
%  Homepage      : http://uvl.udg.edu
%
%  Module        : Pascal Fua's - Like Propagation Algorithm
%
%  File          : MatchPropagationFua.m
%  Date          : 13/02/2008 - 13/02/2008
%  Encoding      : ISO-8859-1 (Latin-1)
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     :
%
%  Notes         :
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2008 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  MatchPropagationFua Match propagation implementation based on Pascal
%                      Fua '91 IJCAI paper: "Combining Stereo and Monocular
%                      Information to Compute Dense Depth Maps that Preserve
%                      Depth Discontinuities.
%
%      Md = MatchPropagationFua ( I, J )
%
%     Input Parameters:
%      I: nxm Image.
%      J: oxp Image.
%      Nx: Correlation 1D Window size.
%      Sx: Search 1D Window size.
%      t: Correlation threshold.
%
%     Output Parameters:
%      Md: Final set of matches I->J in format (Ix, Iy, Jx, Jy)'xl
%


function Md = MatchPropagationFua ( I, J, Nx, Sx, t )
  % Test the input parameters
  error ( nargchk ( 5, 5, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Check Input Images
  if ndims ( I ) ~= 2 || ndims ( J ) ~= 2;
    error ( 'MATLAB:MatchPropagationQuan:Input', ...
            'I (dims = %d), J (dims = %d) must be 2D matrices containing the images!', ...
            ndims ( I ), ndims ( J ) );
  end

  % Calculate Image Sizes
  [Iy Ix] = size ( I );
  [Jy Jx] = size ( J );

  % Scan lines must fit
  if Iy ~= Jy;
    error ( 'MATLAB:MatchPropagationQuan:Input', ...
            'I (%dx%d), J (%dx%d) must share the same sizes!', ...
            Ix, Iy, Jx, Jy );
  end

  % Vertical correlation 
  Ny = 2;

  % c=10;ws=5;cc=conv ( fliplr(Ild(ll,c-ws:c+ws)), Kld(ll,1:30)); [v, i] = max(cc); i - 2*ws-1
  Md = zeros ( Iy, Ix );

  Ir = zeros ( 1, 2*Sx+1 );
  Jr = zeros ( 1, 2*Sx+1 );
  DispScanLine = zeros ( 1, Ix );

  % Scan all epipolar lines
  for i = Ny + 1 : Iy - Ny - 1;
    % Scan the epipolar line
    for j = Nx + 1 : Ix - Nx - 1;
      % Correlate to other image
      l = 1;
      for k = j - Sx : j + Sx;
        % Image Bounds
        if k > Nx && k < Ix - Nx;
          Ir(l) = corr2rectMx ( I, J, j, i, k, i, Nx, Ny );
%          Ir(l) = corr2discMx ( I, J, j, i, k, i, Nx );
        else
          Ir(l) = 0;
        end
        l = l + 1;
      end

      [vIr, IdxIr] = max ( Ir );
      if vIr > t;
        l = 1;
        for k = j - Sx + IdxIr - Sx : j - Sx + IdxIr + Sx;
          % Image Bounds
          if k > Nx && k < Ix - Nx;
            Jr(l) = corr2rectMx ( I, J, j, i, k, i, Nx, Ny );
%            Jr(l) = corr2discMx ( I, J, j, i, k, i, Nx );
          else
            Jr(l) = 0;
          end
          l = l + 1;
        end
      
        [vJr, IdxJr] = max ( Jr );
        if vJr > t;
          di = ( j - Sx + IdxIr );
          dj = ( j - Sx + IdxIr - Sx + IdxJr );
          % One pixel
          d = abs ( di - dj );
          if d <= 1;
            DispScanLine(j) = abs ( ( ( IdxIr - Sx ) + ( IdxIr - 2 * Sx + IdxJr ) ) / 2 );
          else
            DispScanLine(j) = 0;
          end
        else
          DispScanLine(j) = 0;
        end
      else
        DispScanLine(j) = 0;
      end
    end

    % Store Scanline
    Md(i,:) = DispScanLine;

    % Debug Ouptut
    disp ( sprintf ( 'Scanline %d (%5.2f%%)', i, ( i * 100.0 ) / Iy ) );
  end
end
