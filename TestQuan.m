%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : Match Propagation Functions
%  Homepage      : http://uvl.udg.edu
%
%  Module        : Match Propagation Test
%
%  File          : TestQuan.m
%  Date          : 09/02/2008 - 12/02/2008
%  Encoding      : ISO-8859-1 (Latin-1)
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     :
%
%  Notes         :
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2008 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Clean
close all;

% Configuration
SetConfVars;

% Path To Correlation Functions
addpath ( CorrelationPath, '-end' );

% Configuration Flags
DoPlotDisparityMaps = true;      % Show Disparity Maps
DoShowSourceSeedMatches = true;  % Show Source Seed
DoShowFinalSeedMatches = false;  % Show Final Propagated Seeds
DoSaveDisparityMaps = true;      % Save the Disparity Maps in png files
PercetageLimit = 100;            % Percentage Threshold of Matches to stop Match Propagation


% Input Images
LeftImgFilePath = [ DataPath '/' ImageSubFolder{PairN} '/' ImageFiles{PairN,1} ]; %#ok<USENS>
RightImgFilePath = [ DataPath '/' ImageSubFolder{PairN} '/' ImageFiles{PairN,2} ];

% Output Images (Disparity Maps)
DispMapFileNamePrefix = 'dmap_';
IDispMapFilePath = [ ResultsPath '/' DispMapFileNamePrefix ImageFiles{PairN,1} ];
JDispMapFilePath = [ ResultsPath '/' DispMapFileNamePrefix ImageFiles{PairN,2} ];

% Thresholds and parameters for the Match Propagation
disp ( 'Setting up Match Propagation Parameters...' );
RadCorr = MatchPropParams(PairN).RadCorr;   % Correlation Patch Radius.
z = MatchPropParams(PairN).z;               % Correlation Threshold.
Nx = MatchPropParams(PairN).Nx;             % 5 x 5 window around the match.
t = MatchPropParams(PairN).t;               % Intensity Gradient for propagation (connectivity 4).
e = MatchPropParams(PairN).e;               % Eps to consider the Neighborhood of Nx (within the Disparity Gradient).
PercentageLimit = MatchPropParams(PairN).PercentageLimit;


% Load if are not already loaded
disp ( 'Loading...' );
if ~exist ( 'Il', 'var' );
  disp ( sprintf ( '  Image ''%s''...', LeftImgFilePath ) );
  Il = imread ( LeftImgFilePath );
else
  disp ( sprintf ( '  Image ''%s'' already loaded!', LeftImgFilePath ) );
end
if ~exist ( 'Ir', 'var' );
  disp ( sprintf ( '  Image ''%s''...', RightImgFilePath ) );
  Ir = imread ( RightImgFilePath );
else
  disp ( sprintf ( '  Image ''%s'' already loaded!', RightImgFilePath ) );
end
if ~exist ( 'FIMOutput', 'var' );
  if ~exist ( ResultsDataFilePath, 'file' );
    disp ( sprintf ( [ '  Output from FIM in file ''%s'' doesn''t exist!\n' ...
                       '  FIM will be executed to generate the file!\n\n' ...
                       '  Enter to continue or CTRL+C to abort...' ], ...
                       ResultsDataFilePath ) ); %#ok<SPWRN>
    pause;
    RunFIM;
  end
  FIMOutput = load ( ResultsDataFilePath );
end

% FIM Accepted Matches as Seed for Match Propagation
Fl = FIMOutput.ResultsFIM.Img1Features(1:2,FIMOutput.ResultsFIM.AcceptedMatches(1,:));
Fr = FIMOutput.ResultsFIM.Img2Features(1:2,FIMOutput.ResultsFIM.AcceptedMatches(2,:));


% Convert Images to Gray Scale
disp ( '  Converting input Images to Gray Scale in double format...' );
disp ( '    Image Il...' );
if ndims ( Il ) > 2;
  Ild = im2double ( rgb2gray ( Il ) );
else
  Ild = double ( Il ) / double ( max ( Il(:) ) );
end
disp ( '    Image Ir...' );
if ndims ( Il ) > 2;
  Ird = im2double ( rgb2gray ( Ir ) );
else
  Ird = double ( Ir ) / double ( max ( Ir(:) ) );
end

% Match Propagation
disp ( '  Executing match propagation...' );
tic;
[Md Mdi] = MatchPropagationQuan ( Ild, Ird, [Fl; Fr], ...
                                  RadCorr, z, Nx, t, e, PercentageLimit );
t = toc;
disp ( sprintf ( '  Execution time %f s\n', t ) );


disp ( '  Calculating Disparity Maps...' );
disp ( '    Disparity Map Il...' );
% Disparity Map from Image I
dx = Md(3,:) - Md(1,:);
dy = Md(4,:) - Md(2,:);
d = sqrt ( dx .* dx + dy .* dy );
DispMapI = zeros ( size ( Ild ) );
Idx = sub2ind ( size ( Ild ), Md(2,:), Md(1,:) ); 
DispMapI(Idx) = d;

disp ( '    Disparity Map Ir...' );
% Disparity Map from Image J
dx = Mdi(3,:) - Mdi(1,:);
dy = Mdi(4,:) - Mdi(2,:);
d = sqrt ( dx .* dx + dy .* dy );
DispMapJ = zeros ( size ( Ird ) );
Idx = sub2ind ( size ( Ird ), Mdi(2,:), Mdi(1,:) ); 
DispMapJ(Idx) = d;


if DoShowSourceSeedMatches;
  disp ( '  Plotting Matches...' );
  disp ( '    Source Seed Matches over Image Il...' );
  figure; imshow ( Il ); hold on;
  s = sprintf ( 'Source Seed Matches over Image %s (Left)', ImageFiles{PairN,1} );
  title ( s ); set ( gcf, 'Name', s );
  X = Fl(1,:); X(2,:) = NaN;
  Y = Fl(2,:); Y(2,:) = NaN;
  plot ( X, Y, '.', 'Color', 'b' );

  disp ( '    Source Seed Matches over Image Ir...' );
  figure; imshow ( Ir ); hold on;
  s = sprintf ( 'Source Seed Matches over Image %s (Right)', ImageFiles{PairN,2} );
  title ( s ); set ( gcf, 'Name', s );
  X = Fr(1,:); X(2,:) = NaN;
  Y = Fr(2,:); Y(2,:) = NaN;
  plot ( X, Y, '.', 'Color', 'b' );
end


if DoShowFinalSeedMatches;
  disp ( '    Final Seed Matches over Image Il...' );
  figure; imshow ( Il ); hold on;
  s = sprintf ( 'Final Seed Matches over Image %s (Left)', ImageFiles{PairN,1} );
  title ( s ); set ( gcf, 'Name', s );
  X = Md(1,:); X(2,:) = NaN;
  Y = Md(2,:); Y(2,:) = NaN;
  plot ( X, Y, '.', 'Color', 'b' );

  disp ( '    Final Seed Matches over Image Ir...' );
  figure; imshow ( Ir ); hold on;
  s = sprintf ( 'Final Seed Matches over Image %s (Right)', ImageFiles{PairN,2} );
  title ( s ); set ( gcf, 'Name', s );
  X = Mdi(1,:); X(2,:) = NaN;
  Y = Mdi(2,:); Y(2,:) = NaN;
  plot ( X, Y, '.', 'Color', 'b' );
end


if DoPlotDisparityMaps;
  disp ( '  Plotting Disparity Maps...' );

  disp ( '    Disparity Map Il...' );
  % Show Disparity Map from Image I
  DispMapShowI = double ( DispMapI );
  DispMapShowI = DispMapShowI / max ( DispMapShowI(:));
  figure; imshow ( DispMapShowI );
  s = sprintf ( 'Disparity Map Il->Ir over Image %s', ImageFiles{PairN,1} );
  title ( s ); set ( gcf, 'Name', s );

  disp ( '    Disparity Map Ir...' );
  % Show Disparity Map from Image J
  DispMapShowJ = double ( DispMapJ );
  DispMapShowJ = DispMapShowJ / max ( DispMapShowJ(:));
  figure; imshow ( DispMapShowJ );
  s = sprintf ( 'Disparity Map Ir->Il over Image %s', ImageFiles{PairN,2} );
  title ( s ); set ( gcf, 'Name', s );
end


if DoSaveDisparityMaps;
  disp ( '  Saving Disparity Maps...' );
  disp ( sprintf ( '    Image ''%s''...', IDispMapFilePath ) );
  imwrite ( DispMapShowI, IDispMapFilePath );
  disp ( sprintf ( '    Image ''%s''...', JDispMapFilePath ) );
  imwrite ( DispMapShowJ, JDispMapFilePath );
end

