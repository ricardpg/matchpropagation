%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : Match Propagation Functions
%  Homepage      : http://uvl.udg.edu
%
%  Module        : Set Configuration Variables.
%
%  File          : SetConfVars.m
%  Date          : 11/02/2008 - 13/02/2008
%  Encoding      : ISO-8859-1 (Latin-1)
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     :
%
%  Notes         :
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2008 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Path To FIM
FIMPath = '../FIM-2.0';

% Path to Correlation
CorrelationPath = '../Correlation';

% Data Path
DataPath = './Images';

% FIM Output Folder
ResultsPath = './Results';

% FIM Output Workspace
ResultsDataFilePath = [ ResultsPath '/ResultsFIM.mat' ];

% Subfoler relative to 'DataPath' before Image Filename (Diferent Datasets)
ImageSubFolder = {
  'Aloe';
  'Aloe';
  'Venus';
  'Trinocular';
  'Coral'
};

% Set of Images (Left, Right)xn
ImageFiles = { ...
  'view2.png',     'view0.png'; ...          % Aloe Dataset (Original Resolution)
  'view2_low.png', 'view0_low.png'; ...      % Aloe Dataset (Low Resolution)
  'im0.png',       'im2.png'; ...            % Venus Dataset
  'img0_1253.tif', 'img1_1253.tif'; ...      % Trinocular Dataset
  'imgl13801.jpg', 'imgr13801.jpg'           % Coral Dataset
}; 

% MatchPropagation Configuration (Dataset Dependent)
% Lhuillier & Quan TPAMI paper values
MatchPropParams(1).RadCorr = 6;
MatchPropParams(1).z = 0.8;
MatchPropParams(1).Nx = 2;
MatchPropParams(1).t = 0.01;
MatchPropParams(1).e = 1;
MatchPropParams(1).PercentageLimit = 100;

% Same as first dataset
MatchPropParams(2) = MatchPropParams(1);
MatchPropParams(3) = MatchPropParams(1);
MatchPropParams(4) = MatchPropParams(1);

% Less restrictive configuration
MatchPropParams(5).RadCorr = 4;
MatchPropParams(5).z = 0.7;
MatchPropParams(5).Nx = 5;
MatchPropParams(5).t = 0.01;
MatchPropParams(5).e = 6;
MatchPropParams(5).PercentageLimit = 100;

MatchPropParams(3).RadCorr = 3;
MatchPropParams(3).z = 0.65;
MatchPropParams(3).Nx = 2;
MatchPropParams(3).t = 0.02;
MatchPropParams(3).e = 2;
MatchPropParams(3).PercentageLimit = 100;

% Chosen pair of images to work with
PairN = 5;                                   % Image pair to work with
