%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : Match Propagation Functions
%  Homepage      : http://uvl.udg.edu
%
%  Module        : Lhuillier & Quan Match Propagation Algorithm
%
%  File          : MatchPropagationQuan.m
%  Date          : 09/02/2008 - 13/02/2008
%  Encoding      : ISO-8859-1 (Latin-1)
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     :
%
%  Notes         :
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2008 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  MatchPropagationQuan Match propagation implementation based on Lhuillier
%                       and Quan '02 TPAMI paper: "Match Propagation for
%                       Image-Based Modeling and Rendering". A percentage
%                       as a propagation limit have been added.
%                       The Cross-Consistency check is not performed.
%
%      [Md Mdi] = MatchPropagationQuan ( I, J, Ms, RadCorr, z, Nx, t, e
%                                        [, Percentage] )
%
%     Input Parameters:
%      I: nxm Image.
%      J: oxp Image.
%      Ms: Initial 4xk matches in format (Ix, Iy, Jx, Jy)'xk.
%      RadCorr: Radius for correlating disc patches.
%      z: Correlation threshold to consider a right match.
%      Nx: Neightborhood radius around a match.
%      t: Gradient threshold for s(x) when growing to any direction.
%      e: Epsilon to consider the Neighborhood N(x,x') inside the Gradient Limit.
%      Percentage: Percentage [0..100] of matches with respect to number of
%                  elements in the smaller image to stop the propagation.
%
%     Output Parameters:
%      Md: Final set of matches I->J in format (Ix, Iy, Jx, Jy)'xl
%      Mdi: Inverse set of matches J->I in format (Jx, Jy, Ix, Iy)'xl
%

function [Md Mdi] = MatchPropagationQuan ( I, J, Ms, RadCorr, z, Nx, t, e, Percentage )
  % Test the input parameters
  error ( nargchk ( 8, 9, nargin ) );
  error ( nargoutchk ( 1, 2, nargout ) );

  % Check Parameters
  if nargin < 9;
    Percentage = 1;
  else
    if ~isscalar ( Percentage ) || isempty ( Percentage );
      Percentage = 1;
    else
      Percentage = Percentage / 100;
      if Percentage < 0; Percentage = 0; end;
      if Percentage > 1; Percentage = 1; end;
    end
  end

  % Check input sizes
  [NumRows NumInitSeed] = size ( Ms );
  if NumRows ~= 4 || NumInitSeed <= 0;
    error ( 'MATLAB:MatchPropagationQuan:Input', ...
            'Ms must be a 4xn matrix containg pairs of matches in rows: (Ix;Iy;Jx;Jy)xk!' );
  end

  % Check Input Images
  if ndims ( I ) ~= 2 || ndims ( J ) ~= 2;
    error ( 'MATLAB:MatchPropagationQuan:Input', ...
            'I, J must be 2D matrices containing the images!' );
  end

  % Calculate Image Sizes
  [Iy Ix] = size ( I );
  [Jy Jx] = size ( J );
  
  % Block Size when growing the Seed matrix: Increase size only ten times
  % as maximum
  SeedBlockSize = ceil ( max ( [ numel(I) numel(J) ] ) / 10 );

  % Filter initial seeds using Correlation
  Seed = zeros ( NumInitSeed, 5 );
  l = 0;
  for i = 1 : NumInitSeed;
    r = corr2discMx ( I, J, round ( Ms(1,i) ), round ( Ms(2,i) ), round ( Ms(3,i) ), round ( Ms(4,i) ), RadCorr );
    if r > z;
      l = l + 1;
      Seed(l,:) = [ r, round(Ms(:, i))' ];
    end
  end

  % The case that any Initial Seed passed the Correlation Test
  if l > 0;
    % Num of Initial Filtered Seeds
    NumSeed = l;    
  else
    % Nothing to do
    Md = [];
    return;
  end
  
  % All Possible Neighborhood Offsets Around a Seed Match
  Nxx = repmat ( -Nx:Nx, Nx * 2 + 1, 1 );
  Nxxt = Nxx';

  % Map I -> J and Inverse Map J -> I to Store Final Matches
  Mapx = zeros ( size ( I ) );
  Mapy = Mapx;
  Mapxp = zeros ( size ( J ) );
  Mapyp = Mapxp;

  % Store initial seeds in the Map
  Idx = sub2ind ( size ( I ), Seed(1:NumSeed, 3), Seed(1:NumSeed, 2) );
  Mapx ( Idx ) = Seed(1:NumSeed, 4);
  Mapy ( Idx ) = Seed(1:NumSeed, 5);
  Idx = sub2ind ( size ( J ), Seed(1:NumSeed, 5), Seed(1:NumSeed, 4) );
  Mapxp ( Idx ) = Seed(1:NumSeed, 2);
  Mapyp ( Idx ) = Seed(1:NumSeed, 3);

  % Space to Store the Disparity Gradient at Each Iteration
  dg = zeros ( 1, 2 );

  % Space Sufficient for all Possible Matches
  Local = zeros ( ( 2 * Nx + 1 ) ^ 2, 5 );

  % Space for Intermediate vars.
  Ntx = [ 0 0 0 0 ];
  Nty = [ 0 0 0 0 ];
  sun = [ 0 0 0 0 ];

  % Counters (Debug Purposes)
  It = 0;
  MapCnt = NumSeed;
  ItMod = 10000;
  % Number of elements in Images
  NumElemI = numel ( I );
  NumElemJ = numel ( J );
  MinNumElem = min ( [ NumElemI NumElemJ ] );
  t0 = cputime;
  ti = t0;
  % Iterate the Match Propagation (It can be stoped by the percentage)
  while NumSeed > 0 && (MapCnt/MinNumElem) < Percentage;
    % Pull Best Match in Seed
    [r x] = max ( Seed(1:NumSeed, 1) );
    CurrSeed = Seed(x, :);
    % Delete: and move the last to current position
    Seed(x, :) = Seed(NumSeed, :);
%    Seed(NumSeed, :) = [ 0 0 0 0 0 ];
    Seed(NumSeed, 1) = 0;
    NumSeed = NumSeed - 1;

    % Store in Local new candidate matches inforcing the disparity gradient
    % limit
    NumLocal = 0;

    % All possible neighborhood
    Nux = Nxx + CurrSeed(2);
    Nuy = Nxxt + CurrSeed(3);
    Nuxp = Nxx + CurrSeed(4);
    Nuyp = Nxxt + CurrSeed(5);

    for i = 1 : numel ( Nux );
      % Avoid indexing too much
      Nuxi = Nux(i);
      Nuyi = Nuy(i);
      Nuxpi = Nuxp(i);
      Nuypi = Nuyp(i);

      % Check Out of Image bounds
      if Nuxi >= RadCorr && Nuyi >= RadCorr && ...
         Nuxpi >= RadCorr && Nuypi >= RadCorr && ...
         Nuxi <= Ix - RadCorr + 1 && Nuxpi <= Jx - RadCorr + 1 && ...
         Nuyi <= Iy - RadCorr + 1 && Nuypi <= Jy - RadCorr + 1;

       % Calculate Disparity gradient
        dg(1) = ( Nuxpi - Nuxi ) - ( CurrSeed(4) - CurrSeed(2) );
        dg(2) = ( Nuypi - Nuyi ) - ( CurrSeed(5) - CurrSeed(3) );
        % Filter using the disparity gradient using norm infinit: || ||_inf
        if max ( dg ) <= e;
          % Compute Neighborhood to compute confidence measure in image I
          % Neighborhood to compute the gradient s(x) (Connectivity 4)
          Ntx(1) = Nuxi - 1;  Nty(1) = Nuyi;
          Ntx(2) = Nuxi;      Nty(2) = Nuyi - 1;
          Ntx(3) = Nuxi + 1;  Nty(3) = Nuyi;
          Ntx(4) = Nuxi;      Nty(4) = Nuyi + 1;

          % Confidence measure for I
          CurPix = I(Nuyi, Nuxi);
          sun(1) = abs ( I(Nty(1), Ntx(1)) - CurPix );
          sun(2) = abs ( I(Nty(2), Ntx(2)) - CurPix );
          sun(3) = abs ( I(Nty(3), Ntx(3)) - CurPix );
          sun(4) = abs ( I(Nty(4), Ntx(4)) - CurPix );
          su = max ( sun ) > t;

          % Compute Neighborhood to compute confidence measure in image J
          % Neighborhood to compute the gradient s(x) (Connectivity 4)
          Ntx(1) = Nuxpi - 1;  Nty(1) = Nuypi;
          Ntx(2) = Nuxpi;      Nty(2) = Nuypi - 1;
          Ntx(3) = Nuxpi + 1;  Nty(3) = Nuypi;
          Ntx(4) = Nuxpi;      Nty(4) = Nuypi + 1;

          % Confidence measure for J
          CurPix = J(Nuypi, Nuxpi);
          sun(1) = abs ( J(Nty(1), Ntx(1)) - CurPix );
          sun(2) = abs ( J(Nty(2), Ntx(2)) - CurPix );
          sun(3) = abs ( J(Nty(3), Ntx(3)) - CurPix );
          sun(4) = abs ( J(Nty(4), Ntx(4)) - CurPix );
          sup = max ( sun ) > t;

          % Correlation between the possible matches
          zncc = corr2discMx ( I, J, Nuxi, Nuyi, Nuxpi, Nuypi, RadCorr );
          if su && sup && zncc > z;
            NumLocal = NumLocal + 1;
            Local( NumLocal, : ) = [ zncc, Nuxi, Nuyi, Nuxpi, Nuypi ];
          end
        end
      end
    end

    % Store in Seed and Map final matches enforcing the uniqueness constraint
    while NumLocal > 0;
      % Pull Best in Local
      % Delete: and move the last to current position      
      [r, y] = max ( Local(1:NumLocal, 1) );
      CurrSeed = Local(y, :);
      Local(y, :) = Local(NumLocal, :);
      % Local(NumLocal, :) = [ 0 0 0 0 0 ];
      Local(NumLocal, 1) = 0;
      NumLocal = NumLocal - 1;

      % Check Uniqueness in Both Directions
      if Mapx( CurrSeed(1,3), CurrSeed(1,2) ) == 0 && Mapxp ( CurrSeed(1,5), CurrSeed(1,4) ) == 0;
        % Store in Map
        Mapx( CurrSeed(1,3), CurrSeed(1,2) ) = CurrSeed(1,4);
        Mapy( CurrSeed(1,3), CurrSeed(1,2) ) = CurrSeed(1,5);
        Mapxp( CurrSeed(1,5), CurrSeed(1,4) ) = CurrSeed(1,2);
        Mapyp( CurrSeed(1,5), CurrSeed(1,4) ) = CurrSeed(1,3);

        % Store in Seed
        NumSeed = NumSeed + 1;
        if NumSeed == ( size ( Seed, 1 ) + 1 );
          % Increase Available Space in Seed 
          Seed(NumSeed:end+SeedBlockSize, :) = 0;
        end
        Seed(NumSeed, :) = CurrSeed;
        
        % For Debug Purposes
        MapCnt = MapCnt + 1;
      end
    end

    % Show Debug Info
    It = It + 1;
    if mod ( It, ItMod ) == 0;
      tf = cputime;
      dt = tf - ti;
      d0 = tf - t0;
      disp ( sprintf ( 'Iterations %7d:', It ) );
      disp ( sprintf ( '  Seed size: %d; Map (I, J) completeness (%d/%d (%5.2f%%), %d/%d (%5.2f%%))', ...
             NumSeed, MapCnt, NumElemI, MapCnt * 100.0 / NumElemI, MapCnt, NumElemJ, MapCnt * 100.0 / NumElemJ ) );
      disp ( sprintf ( '  Iteration Time %f;  Expected Time at completeness 100%% (%f / %f) s', dt, d0, d0 / ( MapCnt / MinNumElem ) ) );

      % Next Iteration
      ti = cputime;
      drawnow;
    end  
  end

  % Create the Final List of Matches
  Idx = find ( Mapx );
  [Y, X] = ind2sub ( size ( I ), Idx );
  Md = [ X Y Mapx(Idx) Mapy(Idx) ]';

  % If Second Output Parameters is Provided
  if nargout > 1;
    Idx = find ( Mapxp );
    [Y, X] = ind2sub ( size ( J ), Idx );
    Mdi = [ X Y Mapxp(Idx) Mapyp(Idx) ]';
  end
end
